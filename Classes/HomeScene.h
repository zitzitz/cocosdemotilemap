#ifndef HOMESCENE_H
#define HOMESCENE_H

#include "cocos2d.h"

class HomeScene : public cocos2d::Layer
{
public:
    static cocos2d::Scene *createScene();

    virtual bool init();

    CREATE_FUNC(HomeScene);
};

#endif // HOMESCENE_H
