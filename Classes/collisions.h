#ifndef COLLISIONS_H
#define COLLISIONS_H


struct Collisions
{
    Collisions(float _x, float _y, float _width, float _height)
    {
        x = _x;
        y = _y;
        width = _width;
        height = _height;
    }

    float x;
    float y;
    float width;
    float height;
};

#endif // COLLISIONS_H
