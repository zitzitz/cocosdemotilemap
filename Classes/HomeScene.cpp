#include "HomeScene.h"
#include "Define.h"
#include "Character.h"

USING_NS_CC;

Scene* HomeScene::createScene()
{
    auto scene = Scene::create();

    auto layer = HomeScene::create();

    scene->addChild(layer);

    return scene;
}


bool HomeScene::init()
{
    if(!Layer::init() )
    {
        return false;
    }

    auto map = TMXTiledMap::create("inHouse.tmx");

    //auto map = new MapManager(TMXTiledMap::create("inHouse.tmx"));


    std::cout << "CHECK\n";



    auto playerStart = map->getObjectGroup("Sys")->getObject("StartGame");


    this->addChild(map);

    auto sprite = Sprite::create("gfx/character.png", Rect(0,6,16,20));

    //auto charact = new Character(sprite, map);

    //charact->setPosition(Vec2(playerStart["x"].asFloat() + (playerStart["width"].asFloat() / 2), playerStart["y"].asFloat() + (playerStart["height"].asFloat() / 2)));

    return true;
}
