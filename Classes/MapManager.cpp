#include "MapManager.h"
#include "Define.h"
#include <iostream>

USING_NS_CC;

std::vector<Collisions> MapManager::collisions;

MapManager::MapManager(TMXTiledMap* map)
{
    MapManager::collisions.clear();
    this->map = map;
    this->addChild(this->map);

    this->start();
}

TMXTiledMap* MapManager::getMap() const
{
    return this->map;
}

void MapManager::start()
{
    this->map->getLayer("house_u")->setLocalZOrder(99);
    this->map->getLayer("house_d")->setLocalZOrder(2);


    auto objectGroup = this->map->getObjectGroup("Collisions");
            for (auto object : objectGroup->getObjects())
            {
                auto value = object.asValueMap();

                MapManager::collisions.push_back(
                    Collisions(value.at("x").asFloat(), value.at("y").asFloat() + 2,
                              value.at("width").asFloat(), value.at("height").asFloat() + 2));

/*
                //DRAW DEBUG RECT
                if (DEBUG_MODE == 1)
                {
                   auto rect = DrawNode::create();
                    rect->drawRect(
                        Vec2(value.at("x").asFloat(), value.at("y").asFloat()),
                        Vec2(value.at("x").asFloat() + value.at("width").asFloat(), value.at("y").asFloat() + value.at("height").asFloat()),
                        Color4F::RED);

                    //we make margin for the rectangle is above the layer "above"
                    this->map->addChild(rect, 99);
                    //log("%f", value.at("x").asFloat());
                }*/
            }
}


std::vector<Collisions> MapManager::getCollisions()
{
    return MapManager::collisions;
}
