#ifndef MAPMANAGER_H
#define MAPMANAGER_H

#include "cocos2d.h"
#include <vector>

struct Collisions
{
    Collisions(float _x, float _y, float _width, float _height)
    {
        x = _x;
        y = _y;
        width = _width;
        height = _height;
    }

    float x;
    float y;
    float width;
    float height;
};

class MapManager : public cocos2d::Node
{
public:
    MapManager(cocos2d::TMXTiledMap*);
    cocos2d::TMXTiledMap* getMap() const;
    static std::vector<Collisions> getCollisions();
    static std::vector<Collisions> collisions;
private:
    void start();
    cocos2d::TMXTiledMap* map;
};

#endif // MAPMANAGER_H
