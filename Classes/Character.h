#ifndef CHARACTER_H
#define CHARACTER_H

#include "cocos2d.h"
#include "collisions.h"

enum CHARACTER_DIRECTION{
    UP,
    DOWN,
    RIGHT,
    LEFT
};


class Character : public cocos2d::Node
{
public:
    Character(cocos2d::Sprite*, cocos2d::TMXTiledMap*);
    ~Character();
    void setMap(cocos2d::TMXTiledMap*);

    CHARACTER_DIRECTION dir;
    cocos2d::Rect* collisionBox;
    bool isMove;
    std::vector<Collisions> collisions;

private:
    void movement();
    virtual void update(float delta);
    void start();
    void CharacterWalkAnimation(float delta);
    void cameraFollow();
    bool getCollision();
    //void goTrtansition();

    cocos2d::Sprite *sprite;
    cocos2d::TMXTiledMap* map;
    float lastPosition;

    bool hasCollision;
    float speed;
    //std::vector<Collisions> collisions;




};

#endif // CHARACTER_H
