#ifndef GAMESCENE_H
#define GAMESCENE_H

#include "cocos2d.h"
#include "Character.h"
#include "collisions.h"




class GameScene : public cocos2d::Layer
{
public:
    static cocos2d::Scene *createScene();

    virtual bool init();
    virtual void update(float delta);
    virtual void onKeyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event);
    virtual void onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event);
    void initEvents();
    CREATE_FUNC(GameScene);
    std::vector<Collisions> getCollisions();
    void goTransition();
    void cameraFollow();
private:
    void setCollisions();
    Character* character;
    cocos2d::TMXTiledMap* map;
    std::vector<Collisions> collisions;
    bool readyToGo;
    bool inWorldMap;
};

#endif // GAMESCENE_H
