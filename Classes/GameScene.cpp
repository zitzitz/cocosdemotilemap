#include "GameScene.h"
#include "Character.h"
#include <iostream>

USING_NS_CC;


Scene* GameScene::createScene()
{
    auto scene = Scene::create();

    auto layer = GameScene::create();
    layer->setName("larr");

    scene->addChild(layer, 0, 101);

    return scene;
}

bool GameScene::init()
{
    if(!Layer::init())
    {
        return false;
    }
    scheduleUpdate();
    this->initEvents();
    readyToGo = true;
    inWorldMap = true;

    map = TMXTiledMap::create("worldmap.tmx");
    this->map->getLayer("house_u")->setGlobalZOrder(99);
    //this->map->getLayer("house_d")->setLocalZOrder(1);


    auto playerStart = map->getObjectGroup("Sys")->getObject("StartGame");
    //map->setPosition(Vec2(0, -(map->getContentSize().height - Director::getInstance()->getWinSize().height)));

    this->addChild(map, 1);

    auto sprite = Sprite::create("gfx/character.png", Rect(0,6,16,20));

    character = new Character(sprite, map);
    this->setCollisions();
    //map->setPositionY(-playerStart["y"].asFloat()+Director::getInstance()->getWinSize().height);
    character->setPosition(Vec2(playerStart["x"].asFloat() + (playerStart["width"].asFloat() / 2), playerStart["y"].asFloat() + (playerStart["height"].asFloat() / 2)));
    //character->setPosition(100, 100);
    this->addChild(character, 2);
    //this->runAction(Follow::create(character, Rect(0, 0, map->getContentSize().width, map->getContentSize().height)));

    return true;
}

void GameScene::update(float delta)
{
    this->cameraFollow();
    this->goTransition();
}

void GameScene::initEvents()
{

    auto listener = EventListenerKeyboard::create();
        listener->onKeyPressed = CC_CALLBACK_2(GameScene::onKeyPressed, this);
        listener->onKeyReleased = CC_CALLBACK_2(GameScene::onKeyReleased, this);
        _eventDispatcher->addEventListenerWithFixedPriority(listener, 1);
}



void GameScene::onKeyPressed(EventKeyboard::KeyCode keyCode, Event* event)
{

    switch (keyCode)
    {
        case EventKeyboard::KeyCode::KEY_UP_ARROW:
             this->character->dir = UP;
        break;
        case EventKeyboard::KeyCode::KEY_DOWN_ARROW:
             this->character->dir = DOWN;
        break;
        case EventKeyboard::KeyCode::KEY_RIGHT_ARROW:
             this->character->dir = RIGHT;
        break;
        case EventKeyboard::KeyCode::KEY_LEFT_ARROW:
            this->character->dir = LEFT;
        break;
    }

    this->character->isMove = true;

}


void GameScene::onKeyReleased(EventKeyboard::KeyCode keyCode, Event *event)
{
    this->character->isMove = false;
}

void GameScene::goTransition()
{
    //if(this->character->dir == UP)
    if(this->readyToGo)
    {
        if(this->inWorldMap)
        {
            auto playerStart = this->map->getObjectGroup("Transitions")->getObject("toHouse");

            if( this->character->collisionBox->intersectsRect( Rect(playerStart.at("x").asFloat(), playerStart.at("y").asFloat(), playerStart.at("width").asFloat(), playerStart.at("height").asFloat() ) ) && this->character->dir == UP)
            {
                this->map->removeAllChildren();
                //map->autorelease();
                this->inWorldMap = false;

                //this->removeAllChildren();
                character->collisions.clear();
              //  this->map = TMXTiledMap::create("inHouse.tmx");
                map = TMXTiledMap::create("inHouse.tmx");
                this->addChild(map);

                //this->map = TMXTiledMap::create("worldmap.tmx");
              //  this->map->getLayer("house_u")->setLocalZOrder(99);
              //  this->map->getLayer("house_d")->setLocalZOrder(1);
    /*
                auto objectGroup = this->map->getObjectGroup("Collisions");
                    for (auto object : objectGroup->getObjects())
                    {
                        auto value = object.asValueMap();

                        this->collisions.push_back(
                            Collisions(value.at("x").asFloat(), value.at("y").asFloat() + 2,
                                      value.at("width").asFloat(), value.at("height").asFloat() + 2));
                    }*/
                this->setCollisions();
                auto playerStart = map->getObjectGroup("Sys")->getObject("StartGame");
                character->setMap(this->map);
                character->setPosition(Vec2(playerStart["x"].asFloat() + (playerStart["width"].asFloat() / 2), playerStart["y"].asFloat() + (playerStart["height"].asFloat() / 2)));


                readyToGo = false;
            }
        }
        else
        {

        }

    }
    else
    {
        readyToGo = true;
    }
}



void GameScene::cameraFollow()
{

    Size winSize = Director::getInstance()->getWinSize();

    Vec2 position = character->getPosition();
    if(this->inWorldMap)
    {
        int x = MAX(position.x, winSize.width / 2);
        int y = MAX(position.y, winSize.height / 2);
        x = MIN(x, (this->map->getMapSize().width * this->map->getTileSize().width) - winSize.width / 2);
        y = MIN(y, (this->map->getMapSize().height * this->map->getTileSize().height) - winSize.height / 2);
        Vec2 actualPosition = Vec2(x, y);

        Vec2 centerOfView = Vec2(winSize.width / 2, winSize.height / 2);
        Vec2 viewPoint = centerOfView - actualPosition;
        this->setPosition(viewPoint);
    }
    else
    {
        Vec2 actualPosition = Vec2(position.x, position.y);

        Vec2 centerOfView = Vec2(winSize.width / 2, winSize.height / 2);
        Vec2 viewPoint = centerOfView - actualPosition;
        this->setPosition(viewPoint);
    }
}

void GameScene::setCollisions()
{
    auto objectGroup = this->map->getObjectGroup("Collisions");
            for (auto object : objectGroup->getObjects())
            {
                auto value = object.asValueMap();

                character->collisions.push_back(
                    Collisions(value.at("x").asFloat(), value.at("y").asFloat() + 2,
                              value.at("width").asFloat(), value.at("height").asFloat() + 2));

/*
                //DRAW DEBUG RECT
                if (DEBUG_MODE == 1)
                {
                   auto rect = DrawNode::create();
                    rect->drawRect(
                        Vec2(value.at("x").asFloat(), value.at("y").asFloat()),
                        Vec2(value.at("x").asFloat() + value.at("width").asFloat(), value.at("y").asFloat() + value.at("height").asFloat()),
                        Color4F::RED);

                    //we make margin for the rectangle is above the layer "above"
                    this->map->addChild(rect, 99);
                    //log("%f", value.at("x").asFloat());
                }*/
            }


   // auto map = new MapManager(TMXTiledMap::create("worldmap.tmx"));

}
