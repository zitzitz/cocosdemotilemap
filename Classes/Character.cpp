#include "Character.h"
#include <iostream>
#include "HomeScene.h"


USING_NS_CC;

Character::Character(Sprite* sprite, cocos2d::TMXTiledMap* map)
{
    this->sprite = sprite;
    this->map = map;
    this->isMove = false;
    speed = 2.f;
    this->lastPosition = 1;
    this->collisionBox = new Rect(this->getPositionX(), this->getPositionY(), 6, 12);
    this->hasCollision = false;
    this->collisions.clear();
    this->start();
}

Character::~Character()
{
    delete this->collisionBox;
}

void Character::start()
{
    this->addChild(this->sprite);
    scheduleUpdate();
    this->schedule(schedule_selector(Character::CharacterWalkAnimation), 0.11f);
}

void Character::update(float delta)
{
    //this->cameraFollow();
    this->movement();
    this->hasCollision = this->getCollision();
    //this->goTrtansition();
}




void Character::movement()
{
    if (this->isMove)
    {
        Vec2 vec;

        if(this->hasCollision)
            return;

        switch(this->dir)
        {
            case UP:
                this->setPositionY(this->getPositionY() + speed);
                //vec = Vec2(0, 2);
    //            this->map->setPositionY(this->map->getPositionY()-2);
                break;
            case DOWN:
                //vec = Vec2(0, -2);
                this->setPositionY(this->getPositionY() - speed);
      //          this->map->setPositionY(this->map->getPositionY()+2);
                break;
            case RIGHT:
                //vec = Vec2(+ speed, 0);
                this->setPositionX(this->getPositionX() + speed);
        //        this->map->setPositionX(this->map->getPositionX()-2);
                break;
            case LEFT:
          //      vec = Vec2(- speed, 0);
                  this->setPositionX(this->getPositionX() - speed);
          //      this->map->setPositionX(this->map->getPositionX()+2);
        }
        //auto move = MoveBy::create(0.0001, vec);
        //this->runAction(move);

    }
}

void Character::CharacterWalkAnimation(float delta)
{
    if(this->isMove)
    {
        float newPosition = this->lastPosition * 16.f;
        switch (this->dir) {
        case UP:
                this->sprite->setTextureRect(Rect(newPosition, 69.f, 16.f, 23.f));
            break;
        case RIGHT:
                this->sprite->setTextureRect(Rect(newPosition, 38.f, 16.f, 23.f));

            break;
        case DOWN:
            this->sprite->setTextureRect(Rect(newPosition, 6.f, 16.f, 23.f));

            break;
        case LEFT:
            this->sprite->setTextureRect(Rect(newPosition, 102.f, 16.f, 23.f));

            break;
        }
    }

    ++this->lastPosition;
    if (this->lastPosition > 3)
        this->lastPosition = 0;
}


void Character::cameraFollow()
{
    auto scene = Director::getInstance()->getRunningScene();
    auto layer = scene->getChildByTag(101);
    Size winSize = Director::getInstance()->getWinSize();

    Vec2 position = this->getPosition();

    int x = MAX(position.x, winSize.width / 2);
    int y = MAX(position.y, winSize.height / 2);
    x = MIN(x, (this->map->getMapSize().width * this->map->getTileSize().width) - winSize.width / 2);
    y = MIN(y, (this->map->getMapSize().height * this->map->getTileSize().height) - winSize.height / 2);
    Vec2 actualPosition = Vec2(x, y);
    Vec2 centerOfView = Vec2(winSize.width / 2, winSize.height / 2);
    Vec2 viewPoint = centerOfView - actualPosition;
    this->map->setPosition(viewPoint);
    //layer->setPosition(viewPoint);
    //
}

bool Character::getCollision()
{
    float screeWidth = (this->map->getMapSize().width * this->map->getTileSize().width);
    float screeHeight = (this->map->getMapSize().height * this->map->getTileSize().height);

    switch (this->dir)
    {
        case UP:
            this->collisionBox->setRect(this->getPositionX(), this->getPositionY() + 1, 1, 1);
            //if the player is at the border of the windows
            if (this->getPositionY() >= (screeHeight -10))
                return true;
        break;
        case DOWN:
            this->collisionBox->setRect(this->getPositionX(), this->getPositionY() - 1, 1, 2);
            //if the player is at the border of the windows
            if (this->getPositionY() <= 10)
                return true;
        break;
        case RIGHT:
            this->collisionBox->setRect(this->getPositionX() + 3, this->getPositionY(), 1, 0);
            //if the player is at the border of the windows
            if (this->getPositionX() >= (screeWidth - 10))
                return true;
        break;
        case LEFT:
            this->collisionBox->setRect(this->getPositionX() - 3, this->getPositionY(), 1, 0);
            //if the player is at the border of the windows
            if (this->getPositionX() <= 10)
                return true;
        break;
    }

    //check if the character is hit a collision box
    bool hasCollision = false;

    for (auto collision : this->collisions)
    {
        if (this->collisionBox->intersectsRect(Rect(collision.x, collision.y, collision.width, collision.height)))
        {

            hasCollision = true;

            switch (this->dir)
            {
                case UP:
                    this->setPositionY(this->getPositionY()-speed);
                break;
                case DOWN:
                    this->setPositionY(this->getPositionY()+speed);
                break;
                case RIGHT:
                    this->setPositionX(this->getPositionX()-speed);
                break;
                case LEFT:
                    this->setPositionX(this->getPositionX()+speed);
                break;
            }


            break;
        }
        else
            hasCollision = false;
    }

    return hasCollision;
}
/*
void Character::goTrtansition()
{
    if(this->dir == UP)
    {
        auto playerStart = this->map->getObjectGroup("Transitions")->getObject("toHouse");

        if( this->collisionBox->intersectsRect( Rect(playerStart.at("x").asFloat(), playerStart.at("y").asFloat(), playerStart.at("width").asFloat(), playerStart.at("height").asFloat() ) ) )
        {
            this->map->removeAllChildren();
            auto scene = HomeScene::createScene();
            Director::getInstance()->replaceScene(scene);


        }
    }
}
*/
void Character::setMap(TMXTiledMap* map)
{
    this->map = map;
}
